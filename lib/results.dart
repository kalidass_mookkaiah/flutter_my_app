import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultsScore;
  final Function resetHandler;

  Result(this.resultsScore, this.resetHandler);

  String get resultPhrase {
    print('Result scope is ${resultsScore}');
    String resultText = 'You Finished, awesome choices';
    if (resultsScore <= 8) {
      resultText = 'You finished and are ok tools';
    } else if (resultsScore <= 12) {
      resultText = 'You finished and are prefering amazing tools';
    } else if (resultsScore <= 16) {
      resultText = 'You finished and are prefering smashing tools';
    } else {
      resultText = 'You finished and are prefering rocking tools';
    }

    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: [
        Text(
          resultPhrase,
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        FlatButton(
          child: Text('Restart Quiz'),
          textColor: Colors.blue,
          onPressed: resetHandler,
        ),
      ],
    ));
  }
}
