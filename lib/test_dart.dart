/*
class Person{
  String name = 'Kalidass';
  int age = 38;
}
int addNumbers(int num1,int num2){
  print (num1 + num2);
  return num1 + num2;
}

class Person1{
  String name;
  int age;
  
  //Person1({@required String inputName, int age=30}){ // @required understood by flutter
  Person1({  String inputName, int age=30}){
    name = inputName;
    this.age = age;
  }
}

class Person2{
  String name;
  int age;
  
  Person2({this.name = 'kamova', this.age=30});
  
  Person2.veryOld(this.name){
    age=60;
  }
}

void main() {
  for (int i = 0; i < 5; i++) {
    print('hello Kalidass ${i + 1}');
  }
  addNumbers(1,2);
  print('Kalidass First Dart program');
  print(addNumbers(1,2));
  
  var p1 = Person();
  print(p1.name);
  
  var p2 = Person1(inputName: 'Kalidass', age: 39);
  print('${p2.name}  ${p2.age}');
  
  var p3 = Person1(inputName: 'KaMo',);
  print('${p3.name}  ${p3.age}');
  
  var p4 = Person2(name: 'KaModa');
  print('${p4.name}  ${p4.age}');
  
  var p5 = Person2.veryOld('Test Old');
  print('${p5.name}  ${p5.age}');
  
}
*/
