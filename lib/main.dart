import 'package:flutter/material.dart';

import './quiz.dart';
import './results.dart';

// void main(){
//   runApp(MyApp());
// }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScoe = 0;

  final _questions = const [
    {
      'questionText': 'What\'s ur favourite mobile codng Lang?',
      'answers': [
        {'text': 'Flutter', 'score': 9},
        {'text': 'iOS', 'score': 6},
        {'text': 'Andriod', 'score': 4}
      ],
    },
    {
      'questionText': 'What\'s ur favourite UIUX tool?',
      'answers': [
        {'text': 'AdobeXD', 'score': 8},
        {'text': 'figma', 'score': 6},
        {'text': 'inVision', 'score': 4},
      ],
    },
    {
      'questionText': 'What\'s ur favourite middleware?',
      'answers': [
        {'text': 'Kong', 'score': 7},
        {'text': 'MuleSoft', 'score': 7},
        {'text': 'NodeJS', 'score': 6},
        {'text': 'soa', 'score': 4}
      ],
    },
  ];

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScoe = 0;
    });
  }

  void _answerQuestion(int score) {
    if (_questionIndex < _questions.length) {
      _totalScoe += score;
      setState(() {
        _questionIndex = _questionIndex + 1;
      });
    }

    print('Answer Choosen');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Kalidass Flutter Test App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScoe, _resetQuiz),
      ),
    );
  }
}
